const express = require('express');

const router = express.Router();

let dogs = [];

router.get('/add/:name', (req, res) => {

    dogs.push(req.params.name);
    res.status(201).send(dogs);
});

router.get('/remove/:name', (req, res) => {
    dogs = dogs.filter(dog => dog !== req.params.name);
    res.send(dogs);
});

router.get('/all', (req, res) => {
    res.send(dogs);
});


router.get('/clear', (req, res) => {
    dogs = [];
    res.send(dogs);
});


module.exports = router;