const express = require('express');

const DogRoutes = require('./routes/Dogs');


const app = express();

app.use(express.json());

app.use('/dogs', DogRoutes);

app.use('/**', (req, res) => {
    res.status(404).send({message: 'Not found!'});
});



app.listen(5000, () => console.log('Server listening on port 5000.'));